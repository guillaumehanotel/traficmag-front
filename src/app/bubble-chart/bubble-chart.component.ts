import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color } from 'ng2-charts';
import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.css']
})
export class BubbleChartComponent implements OnInit {

  shop;
  updated = false;
  currentIndex = 1;

  public bubbleChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          min: 0,
          max: 24,
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 100,
        }
      }]
    }
  };
  public bubbleChartType: ChartType = 'bubble';
  public bubbleChartLegend = true;
  public bubbleChartData: ChartDataSets[] = [
    {
      data: [
      ],
      label: "Fréquentation du magasin maximale par heure (aujourd'hui)",
    },
  ];

  constructor(private dataService: DataService, private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      if(params.shop === undefined){
        this.router.navigate(['']);
      }
      else {
        var shopId = params.shop;
        this.getShop(shopId);
      }
    });
  }

  getShop(shopId: string){

    this.dataService.getShop(this.userService.currentUserValue.token, shopId).subscribe(shop => {
      this.shop = shop;
      this.getData();
    });
  }

  getData(){

    this.dataService.getShopDay(this.userService.currentUserValue.token, this.shop._id).subscribe(results => {
      var data = [];
      this.bubbleChartOptions.scales.yAxes[0].ticks.max = results.maxNumberPeople - -5;
      results.metadata.forEach(dayResult => {
        let currentPeopleNumber = dayResult.currentPeopleNumber;
        let date = new Date(dayResult.date);
        if(date){
          let hour = date.getHours();
          data.push({ x: hour, y: currentPeopleNumber, r: 5 });
        }
      });

      var newData = [];

      for(var i=0; i<data.length; i++){
        if(this.alreadyContains(data[i], newData)){

          if(data[i].y != 0){
            newData = this.treatDuplicate(data[i], newData);
          }

        } else {

          if(data[i].y != 0){
            newData.push(data[i]);
          }
          
        }
      }

      this.bubbleChartData[0].data = newData;
      this.updated = true;
    });
  }

  treatDuplicate(line, data){

    var x = line.x;

    for(var i=0; i<data.length; i++){
      if(data[i].x == line.x){
        if(data[i].y < line.y){
          data[i].y = line.y;
          break;
        }
      }
    }

    return data;
  }

  alreadyContains(line, data){
    for(var i=0; i<data.length; i++){
      if(line.x == data[i].x){
        return true;
        break;
      }
    }

    return false;
  }
}
