import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl('')
  });

  constructor(private _snackBar: MatSnackBar, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  register(){

    console.log('Login func');

    let name = this.form.value.name;
    let email = this.form.value.email;
    let password = this.form.value.password;
    let confirmPassword = this.form.value.confirmPassword;

    if(email.length < 1 || password.length < 1 || name.length < 1 || email.includes('@') == false || password != confirmPassword){

      this._snackBar.open('Formulaire non valide.', 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    } else {

      this.userService.register(name, email, password).subscribe(result => {
        console.log('Register result : ', result);

        this._snackBar.open('Inscription réussie.', 'Fermer', {
          duration: 3500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
        });

        this.router.navigate(['/login']);
      },

      error => {
        
        this._snackBar.open(error.error.errors.msg[0].msg, 'Fermer', {
          duration: 3500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
        });
      });
    }
  }

}
