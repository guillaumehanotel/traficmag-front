import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-counting',
  templateUrl: './counting.component.html',
  styleUrls: ['./counting.component.css'],
  providers: [DatePipe]
})
export class CountingComponent implements OnInit {

  shop;
  doors = [];
  currentPeopleNumber = 0;
  maxNumberPeople = 0;
  nbAlerts = 0;

  autoMode = false;

  currentDate = new Date();
  formatDate: string = 'dd/MM/yyyy';

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private userService: UserService, private _snackBar: MatSnackBar, public dialog: MatDialog) {}

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      if(params.shop === undefined){
        this.router.navigate(['']);
      }
      else {
        var shopId = params.shop;
        this.getShop(shopId);
      }
    });
  }

  getShop(shopId: string){

    this.dataService.getShop(this.userService.currentUserValue.token, shopId).subscribe(shop => {
      this.shop = shop;
      this.updateCounters();
      this.getDoors();
    });
  }

  getDoors(){
    this.dataService.getDoors(this.userService.currentUserValue.token, this.shop._id).subscribe(doors => {
      this.doors = doors.docs;
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  incrementCounting(){

    this.dataService.incrementShopDayCounter(this.userService.currentUserValue.token, this.shop._id).subscribe(result => {
      this.updateCounters();
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  decrementCounting(){

    this.dataService.decrementShopDayCounter(this.userService.currentUserValue.token, this.shop._id).subscribe(result => {
      this.updateCounters();
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    })
  }

  updateCounters(){

    this.dataService.getTodayCounters(this.userService.currentUserValue.token, this.shop._id).subscribe(results => {
      this.nbAlerts = results.nbAlert;
      this.currentPeopleNumber = results.currentPeopleNumber;
      this.maxNumberPeople = results.maxNumberPeople;
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  deleteDoor(door){

    this.dataService.deleteDoor(this.userService.currentUserValue.token, door._id).subscribe(result => {

      this._snackBar.open('La porte a été supprimée.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });

      setTimeout(()=>{
        window.location.reload();
      }, 1000);
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  openAddDoorDialog(){
    const dialogRef = this.dialog.open(DialogAddDoor, {
      data: {
        shop: this.shop
      }
    });
  }

  mqttCopied(){
    
    this._snackBar.open('Le code Mqtt a été copié.', 'Fermer', {
      duration: 2500,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }

  switchAuto(){
    
    this.autoMode = !this.autoMode;
  }
}

@Component({
  selector: 'dialog-add-door',
  templateUrl: 'dialog-add-door.html',
})
export class DialogAddDoor {
  
  form: FormGroup = new FormGroup({
    name: new FormControl(''),
  });

  constructor(public dialogRef: MatDialogRef<DialogAddDoor>, private _snackBar: MatSnackBar, private dataService: DataService, private userService: UserService, private router: Router, @Inject(MAT_DIALOG_DATA) public data) {}

  addDoor(){
    
    if(this.form.value.name == ""){
      this._snackBar.open('Formulaire non valide.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    } else {

      this.dataService.addDoor(this.userService.currentUserValue.token, this.data.shop._id, this.form.value.name).subscribe(result => {
        this._snackBar.open('La porte a été ajoutée.', 'Fermer', {
          duration: 2500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
        });
  
        setTimeout(()=>{
          window.location.reload();
        }, 1000);
      });
    }
  }

}
