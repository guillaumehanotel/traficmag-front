import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.css']
})
export class ToolbarMenuComponent implements OnInit {

  currentUser;
  userEmail = "non connecté";

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    
    this.currentUser = this.userService.currentUser.subscribe(user => {
      if(user != null){
        this.currentUser = user;
        this.userEmail = user.user.email;
      }
    });
  }

  navigateTo(route: string){

    switch(route){
      case "Home":
        this.router.navigate(['/']);
        break;
      case "Statistics":
        this.router.navigate(['/statistics']);
        break;
      case "About":
        this.router.navigate(['/about']);
        break;
      case "Login":
        this.router.navigate(['/login']);
      default:
        console.log('Route non reconnue.');
        break;
    }
  }

  logout() {
    this.currentUser;
    this.userEmail = "non connecté";
    this.userService.logout();
    this.router.navigate(['login']);
  }

  isLogged() {
    if(this.userService.currentUserValue)
      return true;
    else
      return false;
  }

}
