import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticCountingComponent } from './automatic-counting.component';

describe('AutomaticCountingComponent', () => {
  let component: AutomaticCountingComponent;
  let fixture: ComponentFixture<AutomaticCountingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomaticCountingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticCountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
