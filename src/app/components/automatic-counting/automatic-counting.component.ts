import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogAddDoor } from '../counting/counting.component';

const mqtt = require('mqtt')
let mqttClient;

@Component({
  selector: 'app-automatic-counting',
  templateUrl: './automatic-counting.component.html',
  styleUrls: ['./automatic-counting.component.css']
})
export class AutomaticCountingComponent implements OnInit {

  shop;
  doors = [];
  currentPeopleNumber = 0;
  maxNumberPeople = 0;

  autoMode = false;

  currentDate = new Date();
  formatDate: string = 'dd/MM/yyyy';

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private userService: UserService, private _snackBar: MatSnackBar, public dialog: MatDialog, private cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      if(params.shop === undefined){
        this.router.navigate(['']);
      }
      else {
        var shopId = params.shop;
        this.getShop(shopId);
      }
    });
  }

  getShop(shopId: string){

    this.dataService.getShop(this.userService.currentUserValue.token, shopId).subscribe(shop => {
      this.shop = shop;
      this.getDoors();
    });
  }

  getDoors(){
    this.dataService.getDoors(this.userService.currentUserValue.token, this.shop._id).subscribe(doors => {
      this.doors = doors.docs;
      this.initialiseWebsocket();
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  deleteDoor(door){

    this.dataService.deleteDoor(this.userService.currentUserValue.token, door._id).subscribe(result => {

      this._snackBar.open('La porte a été supprimée.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });

      setTimeout(()=>{
        window.location.reload();
      }, 1000);
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

  openAddDoorDialog(){
    const dialogRef = this.dialog.open(DialogAddDoor, {
      data: {
        shop: this.shop
      }
    });
  }

  mqttCopied(){
    
    this._snackBar.open('Le code Mqtt a été copié.', 'Fermer', {
      duration: 2500,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }

  initialiseWebsocket(){

    mqttClient = mqtt.connect('ws://dawndash.ovh:1884');

    mqttClient.on('connect', () => {
      console.log('connected to broker...');
      this.doors.forEach(door => {
        mqttClient.subscribe(door.mqttTopicName)
      });
    });

    mqttClient.on('message', async (topic, codedMessage) => {

      var message = new TextDecoder().decode(codedMessage);
      
      if(message === "INCREASE") {
        this.currentPeopleNumber -= -1;
        if(this.currentPeopleNumber > this.maxNumberPeople)
          this.maxNumberPeople -= -1;
        this.cdRef.detectChanges();
      }
      else if(message === "DECREASE") {
        this.currentPeopleNumber -= 1;
      }
    });
  }

}
