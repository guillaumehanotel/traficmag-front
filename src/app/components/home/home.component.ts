import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  token: string;
  userId: string;

  shops = [];

  constructor(private router: Router, private userService: UserService, private dataService: DataService, public dialog: MatDialog) { }

  ngOnInit(): void {

    this.dataService.getShops(this.userService.currentUserValue.token, this.userService.currentUserValue.user._id).subscribe(shops => {
      this.shops = shops.docs;
      this.shops.forEach(shop => {
        this.dataService.getDoors(this.userService.currentUserValue.token, shop._id).subscribe(doors => {
          shop.doors = doors.docs;
        });
      });
    })
  }

  goCounting(shop: any){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "shop": shop._id
      }
    };
    this.router.navigate(['/counting'], navigationExtras);
  }

  openAddDialog() {
    const dialogRef = this.dialog.open(DialogAddShop);
  }

  deleteShop(shop: any){
    const dialogRef = this.dialog.open(DialogDeleteShop, {
      data: {
        shop: shop
      }
    });
  }

  goStatistics(shop: any){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "shop": shop._id
      }
    };
    this.router.navigate(['/statistics'], navigationExtras);
  }
}

@Component({
  selector: 'dialog-delete-shop',
  templateUrl: 'dialog-delete-shop.html',
})
export class DialogDeleteShop {

  constructor(public dialogRef: MatDialogRef<DialogAddShop>, @Inject(MAT_DIALOG_DATA) public data, private _snackBar: MatSnackBar, private dataService: DataService, private userService: UserService, private router: Router) {}

  deleteShop(){
    this.dataService.deleteShop(this.userService.currentUserValue.token, this.data.shop._id).subscribe(result => {

      this._snackBar.open('Le magasin a été supprimé.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });

      setTimeout(()=>{
        window.location.reload();
      }, 1000);
    },
    
    error => {

      this._snackBar.open(error.error, 'Fermer', {
        duration: 3500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
        },
      );
    });
  }

}

@Component({
  selector: 'dialog-add-shop',
  templateUrl: 'dialog-add-shop.html',
})
export class DialogAddShop {
  
  form: FormGroup = new FormGroup({
    name: new FormControl(''),
    area: new FormControl(''),
    city: new FormControl(''),
    postalCode: new FormControl(''),
  });

  constructor(public dialogRef: MatDialogRef<DialogAddShop>, private _snackBar: MatSnackBar, private dataService: DataService, private userService: UserService, private router: Router) {}

  addShop(){
    
    if(this.form.value.name == "" || this.form.value.area == "" || this.form.value.city == "" || this.form.value.postalCode == "" || !(this.isANumber(this.form.value.area))){
      this._snackBar.open('Formulaire non valide.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    } else {

      this.dataService.createShop(this.userService.currentUserValue.token, this.userService.currentUserValue.user._id, this.form.value.name, this.form.value.area, this.form.value.city, this.form.value.postalCode).subscribe(result => {

        this._snackBar.open('Le magasin a été créé.', 'Fermer', {
          duration: 2500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
        });

        setTimeout(()=>{
          window.location.reload();
        }, 1000);

      },
      
      error => {

        this._snackBar.open(error.error, 'Dismiss', {
          duration: 3500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
          },
        );
      });
    }
  }

  isANumber(str){
    return !/\D/.test(str);
  }

}
