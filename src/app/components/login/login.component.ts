import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private _snackBar: MatSnackBar, private userService: UserService , private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    console.log('Login func');

    let email = this.form.value.email;
    let password = this.form.value.password;

    if(email.length < 1 || password.length < 1 || email.includes('@') == false){

      this._snackBar.open('Formulaire non valide.', 'Fermer', {
        duration: 2500,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    } else {

      this.userService.login(email, password).subscribe(result => {

        this._snackBar.open('Connexion réussi, redirection...', 'Fermer', {
          duration: 3500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
          },
        );

        this.router.navigate(['/']);

      },
      
      error => {

        this._snackBar.open(error.error.errors.msg, 'Fermer', {
          duration: 3500,
          horizontalPosition: 'right',
          verticalPosition: 'top'
          },
        );
      });
    }
  }

  register(){

    console.log('Register func');

    this.router.navigate(['/register']);
  }

}
