import { Injectable } from '@angular/core';
import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';  

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private serverUrl = `socketServerURL`;
	private stompClient;
  public mapEndpointSubscription: Map<string, any> = new Map();

  constructor() { }

  public async initWebSocket(){

    return new Promise<void>((resolve) => {

      if(!this.stompClient){
        const ws = new SockJS(this.serverUrl);
        this.stompClient = Stomp.Stomp.over(ws);
        this.stompClient.connect({}, resolve);
      } else {
        resolve();
      }
    });
  }

  public async subscribe(name: string, fnc: (event) => void){

    const subscription = this.stompClient.subscribe(`/${name}`, (event) => {
      fnc({...event, body: JSON.parse(event.body)});
    });
    this.mapEndpointSubscription.set(name, subscription);
  }

  public async unsubscribeToWebSocketEvent(name: string){

    const subscription = this.mapEndpointSubscription.get(name);
    if(subscription)
      subscription.unsubscribe();
  }

  public send(name: string, body: any){
    this.stompClient.send(`/app/socket/${name}`, {}, JSON.stringify(body));
  }

  public disconnect(){
    if (this.stompClient !== null)
      this.stompClient.disconnect();
  }
}
