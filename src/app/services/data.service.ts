import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiUrl: string = 'http://traficmag-api.dawndash.ovh';

  constructor(private http: HttpClient) { }

  getShops(token: string, userId: string): Observable<any> {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shops?user=' + userId, {headers: headers});
  }

  createShop(token: string, userId: string, name: string, area: number, city: string, postalCode: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.post<any>(this.apiUrl + '/shops', {user: userId, name: name, area: area, city: city, postal_code: postalCode}, {headers: headers});
  }

  deleteShop(token: string, idShop: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.delete<any>(this.apiUrl + '/shops/' + idShop, {headers: headers});
  }

  incrementShopDayCounter(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shopdaycounter/today/increase?shop=' + shopId, {headers: headers});
  }

  decrementShopDayCounter(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shopdaycounter/today/decrease?shop=' + shopId, {headers: headers});
  }

  getTodayCounters(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shopdaycounter/today?shop=' + shopId, {headers: headers});
  }

  deleteDoor(token: string, doorId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.delete<any>(this.apiUrl + '/doors/' + doorId, {headers: headers});
  }

  addDoor(token: string, shopId: string, name: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.post<any>(this.apiUrl + '/doors', {shop: shopId, name: name}, {headers: headers});
  }

  getDoors(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/doors?shop=' + shopId, {headers: headers});
  }

  getShopDay(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shopdaycounter/today?shop=' + shopId, {headers: headers});
  }

  getShop(token: string, shopId: string): Observable<any>{

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.get<any>(this.apiUrl + '/shops/' + shopId, {headers: headers});
  }
}
